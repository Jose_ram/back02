package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.service.ProductoMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoMongoService productoService;

    @GetMapping("")
    public String index(){
        return "API REST Tech U! v2.0.0";
    }

    //GET a todos los productos (Coleccion)
    @GetMapping("/products")
    public List<ProductoModel> getProductos(){
        return productoService.findAll();
    }

    //POST para crear un producto
    @PostMapping("/products")
    public ProductoModel postProducto(@RequestBody ProductoModel newProduct){
        productoService.save(newProduct);
        return newProduct;
    }

    //GET a un producto concreto por id (Instancia)
    @GetMapping("/products/{id}")
    public Optional<ProductoModel> getProductoById(@PathVariable String id){
        // Se puede comprobar si existe con un existById
        return productoService.findById(id);
    }

    @PutMapping("/products") //por el metodo que vamos a usar permite no pasarle el id por parametro
    public void putProducto(@RequestBody ProductoModel productoToUpdate){
        productoService.save(productoToUpdate);
    }

    @DeleteMapping("/products")
    public boolean deleteProducto(@RequestBody ProductoModel productoToDelete){
        return productoService.delete(productoToDelete);
    }
}
