package com.techu.backend.service;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoMongoService {

    @Autowired
    ProductoRepository productoRepository;

    //READ
    // llamamos a este metodo igual que el de la clase padre de mongorepository
    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    // CREATE o SOBRESCRIBIR
    public ProductoModel save(ProductoModel newProducto){
        return productoRepository.save(newProducto);
    }

    // READ por Id
    public Optional<ProductoModel> findById(String id){
        return productoRepository.findById(id);
    }

    // DELETE
    public boolean delete(ProductoModel productoModel){
        //la excepcion controla que no haya error de escritura,no q este o no el reg
        try{
            productoRepository.delete(productoModel);
            return true;
        } catch (Exception ex){
            return false;
        }
    }
}
